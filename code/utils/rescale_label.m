function [Yscaled]=rescale_label(X_test,Y_test,model,Yavg,Ysd)
%% Re-scale the predicted rates according to Gaussian probability distribution

Yhat = liblinear_predict(Y_test,X_test, model);

%% Construct Gausian prior
Ysd(Ysd==0) = sqrt(eps);
probY = normpdf(Yhat,Yavg,Ysd);
probYavg = 0.35;

%% Rescale the prediction probYavg
Yscaled = (probY.*Yhat + 0.35.*Yavg)./(probY+0.35);


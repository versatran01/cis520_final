function Yhat = predict_libsvm( model, Xtest, Xtrain, kernel )
N = size(Xtest, 1);

if kernel == 4
    kernel_fun = @(x, x2) fastkern(x', x2');
    Ktest = double(kernel_fun(Xtrain, Xtest));
    Yhat = svmpredict(ones(N, 1), add_first_col(Ktest), model, '-b 1');    
else
    Yhat = svmpredict(ones(N, 1), Xtest, model, '-b 1');
end

end
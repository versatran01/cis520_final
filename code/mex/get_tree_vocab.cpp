/*
 * get_tree_vocab.cpp
 *
 *   Copyright (c) 2013 Gareth Cross. All rights reserved. MIT License
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 * and associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 *	Created on: 11/27/2013
 *		Author: Gareth Cross
 */
 
#include "TreeNode.hpp"

void mexFunction( int nlhs, mxArray *plhs[],
                  int nrhs, const mxArray *prhs[] )
{
    //  check inputs
    if(nrhs != 1)
    {
        mexErrMsgIdAndTxt(  "get_tree_vocab:invalidNumInputs",
                            "One input required.");
    }
    else if (nlhs != 1)
    {
        mexErrMsgIdAndTxt(  "get_tree_vocab:invalidNumOutputs",
                            "One output required.");
    }
    else if(!mxIsStruct(prhs[0]))
    {
        mexErrMsgIdAndTxt(  "get_tree_vocab:inputNotStruct",
                            "Input must be a struct.");
    }
    
    TreeNode * tree = new TreeNode();
    try {
        tree->load_mex_struct(prhs[0]);
    }
    catch (std::exception& e) {
        delete tree;
        mexErrMsgIdAndTxt("get_tree_vocab:invalidInput", "Failed to parse tree: %s", e.what());
    }
    
    //  pull all the vocab out of the tree
    std::vector<std::string> vocab;
    tree->extract_tokens(vocab);
    //mexPrintf("%lu vocabulary terms loaded\n", vocab.size());
    
    //  put the vocab in a cell array
    mwSize vocab_size = vocab.size();
    plhs[0] = mxCreateCellArray(1, &vocab_size);
    
    mwIndex idx=0;
    for (std::vector<std::string> :: iterator i = vocab.begin(); i != vocab.end(); i++)
    {
        mxArray * vstring = mxCreateString(i->c_str());
        mxSetCell(plhs[0], idx++, vstring);
    }
    
    //  cleanup
    delete tree;
}

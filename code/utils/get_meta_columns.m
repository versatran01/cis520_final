function [Z, indices] = get_meta_columns(metadata, thresh)

Z = zeros(numel(metadata), 3);

% pull all cool/funny/useful review scores
for i=1:numel(metadata) 
    Z(i,:) = [metadata(i).cool metadata(i).funny metadata(i).useful];
end

% threshold away useless ones
indices = sum(Z,2) >= thresh;
Z = Z(indices, :);

end

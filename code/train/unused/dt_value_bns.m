function [p] = dt_value_bns(t, x)
% DT_VALUE_BNS - Get the value of the decision tree node for input 'x'.
%
% Usage:
%
%   P = DT_VALUE(T, X)
%
% Returns the predicted label for observation X.
%

node = t; % Start at root
while ~node.terminal
    if x(1, node.fidx) > 0
        node = node.left;
    else
        node = node.right;
    end
end
p = node.value;

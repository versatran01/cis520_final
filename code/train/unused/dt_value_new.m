function [y] = dt_value_new(t, x)
% DT_VALUE - Get the value of the decision tree node for input.
%
% Usage:
%
%   Y = DT_VALUE_NEW(T, X)
%

node = t; % Start at root
while ~node.terminal
    if x(node.fidx) == 0
        node = node.left;
    else
        node = node.right;
    end
end
y = node.value;

startup;

tic;
reviews = get_all_review_texts(metadata);
[tree,N] = create_ngram_tree(reviews);
fprintf('%f seconds to map all validation samples\n', toc);

tic;
all_vocab = get_tree_vocab(tree);
fprintf('%f seconds to pull all vocab from big tree\n', toc);

FS = FeatureSelector.train(Xd_train, Ytrain, ...
                           'thresh_bns', 0.07,...
                           'scale', 'none');

feat_ind = FS.feat_ind;
                       
tic;
t = CTimeleft(numel(Mvalid));
for i=1:numel(Mvalid)
    %t.timeleft();
    meta_text = Mvalid(i).text;
    
    % create single tree
    [item_tree,N2] = create_ngram_tree({meta_text});
    if ~isstruct(item_tree)
        continue;
    end
    
    % get feature-space and indices
    
    feat = get_tree_featurespace(item_tree, 1, N2);
    
    local_vocab = get_tree_vocab(item_tree);
    
    [a,b] = tree_cull_vocab_v(item_tree, all_vocab(feat_ind));
    
   % fprintf('iteration: %f\n', toc);
    
    % map
    %x_mapped = zeros(1,numel(idx_train_in_valid));
    %x_mapped(1,idx_train_in_valid) = feat(1,idx_valid_in_train);
    
end

time_taken = toc;
fprintf('%f seconds to map all validation samples\n', time_taken);

/*
 * tree_cull_vocab.cpp
 *
 *   Copyright (c) 2013 Gareth Cross. All rights reserved. MIT License
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 * and associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 *	Created on: 11/27/2013
 *		Author: Gareth Cross
 */
 
#include "TreeNode.hpp"

void mexFunction( int nlhs, mxArray *plhs[],
                  int nrhs, const mxArray *prhs[] )
{
    //  check inputs
    if(nrhs != 2)
    {
        mexErrMsgIdAndTxt("tree_cull_vocab:invalidNumInputs",
                          "Two inputs required.");
    }
    else if (nlhs != 2)
    {
        mexErrMsgIdAndTxt("tree_cull_vocab:invalidNumOutputs",
                          "Two outputs required.");
    }
    else if(!mxIsStruct(prhs[0]) || !mxIsStruct(prhs[1]))
    {
        mexErrMsgIdAndTxt("tree_cull_vocab:inputNotStruct",
                          "Both inputs must be structs.");
    }
    
    //  load both trees
    TreeNode * lTree = new TreeNode();
    TreeNode * rTree = new TreeNode();
    try {
        lTree->load_mex_struct(prhs[0]);
        rTree->load_mex_struct(prhs[1]);
    }
    catch (std::exception& e) {
        delete lTree;
        delete rTree;
        mexErrMsgIdAndTxt("tree_cull_vocab:invalidInput", "Failed to parse tree: %s", e.what());
    }
    
    //  get vocab of trees
    //  Note this is inefficient since all we need is the node count, not the actual voca
    std::vector<std::string> rVocab;
    rTree->extract_tokens(rVocab);
    
    //mexPrintf("Loading %lu terms from right tree. Culling...\n", rVocab.size());
    //mexEvalString("drawnow;");  //  force flush of IO
    
    //  stick results in logical array
    mwSize dim = rVocab.size();
    mxArray * log_right = mxCreateLogicalArray(1, &dim);
    
    dim = lTree->count_nodes();
    mxArray * log_left = mxCreateLogicalArray(1, &dim); //  dimension of left tree vocab

    mxLogical * l_data = static_cast<mxLogical*>(mxGetData(log_left));
    mxLogical * r_data = static_cast<mxLogical*>(mxGetData(log_right));
    
    size_t count=0,kept=0;
    for (std::vector<std::string> :: iterator i = rVocab.begin(); i != rVocab.end(); i++)
    {
        bool found;
        int order =0;
        TreeNode * n = lTree->find(*i, found, order);
        
        if (found) {
            l_data[n->column] = 1;
            r_data[count] = 1;
            kept++;
        }
        
        count++;
    }
    
    //mexPrintf("%lu terms remain.\n", kept);
    
    //  return logical array
    plhs[0] = log_left;
    plhs[1] = log_right;
    
    //  cleanup
    delete lTree;
    delete rTree;
}

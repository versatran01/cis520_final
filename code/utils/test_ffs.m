
fs = FeatureSelector.train(Xtrain,Ytrain,'scale','bns','thresh_bns',0.15,'binary',false);

Xgood = fs.apply(Xvalid);
Xtest = zeros(size(Xgood));

T = CTimeleft(size(Xvalid,1));
for i=1:size(Xvalid,1)
    T.timeleft();
    
    Xtest(i,:) = fast_featureselect(fs, Xvalid(i,:));
    
end

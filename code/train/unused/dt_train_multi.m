function root = dt_train_multi(X, Y, depth_limit)
% DT_TRAIN_MULTI - Trains a multi-class decision tree classifier.
%
% Usage:
%
%    tree = dt_train(X, Y, DEPTH_LIMIT)
%
% Returns a tree of maximum depth DEPTH_LIMIT learned using the ID3
% algorithm. Assumes X is a N x D matrix of N examples with D features. Y
% must be a N x 1 {1, ..., K} vector of labels. 
%
% Returns a linked hierarchy of structs with the following fields:
%
%   node.terminal - whether or not this node is a terminal (leaf) node
%   node.fidx, node.fval - the feature based test (is X(fidx) <= fval?)
%                          associated with this node
%   node.value - 1 x K vector of P(Y==K) as predicted by this node
%   node.left - child node struct on left branch (f <= val)
%   node.right - child node struct on right branch (f > val)
%
% SEE ALSO
%    DT_CHOOSE_FEATURE_MULTI, DT_VALUE

% Basically just copied dt_train and changed the relevant bits...

% Pre-compute the range of each feature.
for i = 1:size(X, 2)
    Xrange{i} = unique(X(:,i));
end

% Build the Z array
% Here we assume the labels take on the form 1,2,3..K
possible_z = unique(Y);

Z = zeros(size(Y,1), size(possible_z,1));

for i = 1:size(Z,1)
    Z(i, Y(i)) = 1;
end

% Recursively split data to build tree.
% Here we pass the mean of Z, which is a row with the mean of each column
root = split_node(X, Z, Xrange, mean(Z), 1:size(Xrange, 2), 0, depth_limit);

%   END OF DT_TRAIN_MULTI
end

function [node] = split_node(X, Z, Xrange, default_value, colidx, depth, depth_limit)
% Utility function called recursively; returns a node structure.
%    
%  [node] = split_node(X, Z, Xrange, default_value, colidx, depth, depth_limit)
%  
%  inputs: 
%    Xrange - cell array containing the range of values for each feature
%    default_value - the default value of the node if Z is empty
%    colidx - the indices of features (columns) under consideration
%    depth - current depth of the tree
%    depth_limit - maximum depth of the tree

%   Calculate the number of labels left to sort
num_labels = nnz(sum(Z));

% The various cases at which we will return a terminal (leaf) node:
%    - we are at the maximum depth
%    - we have only one label left
%    - we have only a single (or no) examples left
%    - we have no features left to split on

if depth == depth_limit || num_labels == 1 || size(Z,1) <= 1 || numel(colidx) == 0
    node.terminal = true;
    node.fidx = [];
    node.fval = [];
    if size(Z,1) == 0
        node.value = default_value;
    else
        node.value = mean(Z);
    end
    node.left = []; node.right = [];

  %  fprintf('depth %d [%d/%d]: Leaf node: = %s\n', depth, sum(Y==0), sum(Y==1), ...
   %     mat2str(node.value));
    return;
end

node.terminal = false;

% Choose a feature to split on using information gain.
[node.fidx node.fval max_ig] = dt_choose_feature_multi(X, Z, Xrange, colidx);

% Remove this feature from future consideration.
colidx(colidx==node.fidx) = [];

% Split the data based on this feature.
leftidx = find(X(:,node.fidx)<=node.fval);
rightidx = find(X(:,node.fidx)>node.fval);

% Store the value of this node in case we later wish to use this node as a
% terminal node (i.e. pruning.)
node.value = mean(Z);

%fprintf('depth %d\n', depth);

% Recursively generate left and right branches.
node.left = split_node(X(leftidx, :), Z(leftidx, :), Xrange, node.value, colidx, depth+1, depth_limit);
node.right = split_node(X(rightidx, :), Z(rightidx, :), Xrange, node.value, colidx, depth+1, depth_limit);

end

/*
 * strorder.c
 *
 *   Copyright (c) 2013 Gareth Cross. All rights reserved. MIT License
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 * and associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 *	Created on: 11/29/2013
 *		Author: Gareth Cross
 */
 
#include "mex.h"
#include "string.h"

void mexFunction( int nlhs, mxArray *plhs[],
                  int nrhs, const mxArray *prhs[] )
{
    //  check inputs
    if(nrhs != 2)
    {
        mexErrMsgIdAndTxt("strorder:invalidNumInputs",
                          "Two inputs required.");
    }
    else if (nlhs != 1)
    {
        mexErrMsgIdAndTxt("fastkern:invalidNumOutputs",
                          "One output required.");
    }
    else if(!mxIsChar(prhs[0]) || !mxIsChar(prhs[1]))
    {
        mexErrMsgIdAndTxt("fastkern:inputNotStruct",
                          "Both inputs must be strings.");
    }
    
    const char * c1 = mxArrayToString(prhs[0]);
    const char * c2 = mxArrayToString(prhs[1]);
    
    int result = strcmp(c1, c2);
    
    mxFree((void *)c1);
    mxFree((void *)c2);
    
    //  done
    plhs[0] = mxCreateDoubleScalar((double)result);
}


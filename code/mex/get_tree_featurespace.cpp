/*
 * get_tree_featurespace.cpp
 *
 *   Copyright (c) 2013 Gareth Cross. All rights reserved. MIT License
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 * and associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 *	Created on: 11/27/2013
 *		Author: Gareth Cross
 */
 
#include "TreeNode.hpp"

void mexFunction( int nlhs, mxArray *plhs[],
                  int nrhs, const mxArray *prhs[] )
{
    //  check inputs
    if(nrhs != 3)
    {
        mexErrMsgIdAndTxt(  "get_tree_featurespace:invalidNumInputs",
                            "Three inputs required.");
    }
    else if (nlhs != 1)
    {
        mexErrMsgIdAndTxt(  "get_tree_featurespace:invalidNumOutputs",
                            "One output required.");
    }
    else if(!mxIsStruct(prhs[0]))
    {
        mexErrMsgIdAndTxt(  "get_tree_featurespace:inputNotStruct",
                            "First tnput must be a struct.");
    }
    else if (!IS_SCALAR(prhs[1]) || !IS_SCALAR(prhs[2]))
    {
        mexErrMsgIdAndTxt("get_tree_featurespace:inputNotNumeric",
                          "Second/third inputs must be scalar dimensions.");
    }
    
    mwSize m = static_cast<mwSize>(mxGetScalar(prhs[1]));   //  number of rows in X (# of observations!)
    mwSize n = static_cast<mwSize>(mxGetScalar(prhs[2]));   //  number of columns in X (size of vocab!)
    
    if (!m || !n) {
        mexErrMsgIdAndTxt("get_tree_featurespace:inputNotFinite",
                          "Second/third inputs must be non zero!");
    }
    
    TreeNode * tree = new TreeNode();
    try {
        tree->load_mex_struct(prhs[0]);
    }
    catch (std::exception& e) {
        delete tree;
        mexErrMsgIdAndTxt("get_tree_featurespace:invalidInput", "Failed to parse tree: %s", e.what());
    }

    //  get # of observations, this is the number of filled spaces in the sparse matrix (avoid mxRealloc later)
    size_t num_observations = tree->count_observations();
    
    //mexPrintf("Generating %lu x %lu sparse matrix, with %lu instances (%.5f %%)\n", m, n, num_observations, num_observations*100.0f / ((float)m*n));
    
    //  fill the sparse array
    mxArray * sparse = mxCreateSparse(m,n,num_observations,mxREAL);
    
    double * d = mxGetPr(sparse);
    mwIndex * ir = mxGetIr(sparse);
    mwIndex * jc = mxGetJc(sparse);
        
    mwIndex idx=0, start_col=0;
    tree->extract_features(d, ir, jc, idx, start_col);
    jc[start_col] = idx;  //  append last index
    
    //mexPrintf("Traversed tree, loading %lu instances into %lu columns.\n", idx, start_col);
    
    //  return sparse array
    plhs[0] = sparse;
    
    //  cleanup
    delete tree;
}


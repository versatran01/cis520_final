/*
 * tree_cull_vocab_v.c
 *
 *   Copyright (c) 2013 Gareth Cross. All rights reserved. MIT License
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 * and associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 *	Created on: 12/2/2013
 *		Author: Gareth Cross
 */

#include "TreeNode.hpp"

void mexFunction( int nlhs, mxArray *plhs[],
                 int nrhs, const mxArray *prhs[] )
{
    //  check inputs
    if(nrhs != 2)
    {
        mexErrMsgIdAndTxt("tree_cull_vocab_v:invalidNumInputs",
                          "Two inputs required.");
    }
    else if (nlhs != 2)
    {
        mexErrMsgIdAndTxt("tree_cull_vocab_v:invalidNumOutputs",
                          "Two outputs required.");
    }
    else if(!mxIsStruct(prhs[0]))
    {
        mexErrMsgIdAndTxt("tree_cull_vocab_v:inputNotStruct",
                          "First inputs must be struct.");
    }
    else if (!mxIsCell(prhs[1]))
    {
        mexErrMsgIdAndTxt("tree_cull_vocab_v:inputNotStruct",
                          "Second input must be cell array of strings.");
    }
    
    size_t num_vocab = mxGetNumberOfElements(prhs[1]);
    if (num_vocab == 0) {
        mexErrMsgIdAndTxt("create_ngram_tree_v:invalidInput",
                          "There must be more than 0 vocabulary terms.");
    }
    
    //  load left trees
    TreeNode * lTree = new TreeNode();
    try {
        lTree->load_mex_struct(prhs[0]);
    }
    catch (std::exception& e) {
        delete lTree;
        mexErrMsgIdAndTxt("tree_cull_vocab_v:invalidInput", "Failed to parse tree: %s", e.what());
    }
    
    //mexPrintf("Loading %lu terms from right tree. Culling...\n", num_vocab);
    //mexEvalString("drawnow;");  //  force flush of IO
    
    //  stick results in logical array
    mwSize dim = num_vocab;
    mxArray * log_right = mxCreateLogicalArray(1, &dim);    //  dimension of num_vocab
    
    dim = lTree->count_nodes();
    mxArray * log_left = mxCreateLogicalArray(1, &dim); //  dimension of left tree vocab
    
    //mexPrintf("%lu terms in left tree\n", dim);
    
    mxLogical * l_data = static_cast<mxLogical*>(mxGetData(log_left));  //  initialized to false
    mxLogical * r_data = static_cast<mxLogical*>(mxGetData(log_right));
    
    size_t kept=0;
    for (mwIndex i = 0; i < num_vocab; i++)
    {
        //  pull string
        mxArray * cell = mxGetCell(prhs[1], i);
        
        if (!mxIsChar(cell)) {
            delete lTree;   //  make sure tree is destroyed
            mexErrMsgIdAndTxt("create_ngram_tree_v:invalidInput",
                              "Second input must be a cell array of strings.");
        }
        
        char * cstr = mxArrayToString(cell);
        std::string term = std::string(cstr);
        mxFree(cstr);
        
        bool found;
        int order = 0;
        TreeNode * n = lTree->find(term, found, order);
        if (found) {
            l_data[n->column] = 1;
            r_data[i] = 1;
            kept++;
        }
    }
    //mexPrintf("%lu terms in tree\n", c1);
    
    //mexPrintf("%lu terms remain.\n", kept);
    
    //  return logical array
    plhs[0] = log_left;
    plhs[1] = log_right;
    
    //  cleanup
    delete lTree;
}

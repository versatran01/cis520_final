function [ Y_pred ] = predict_dt(model, X_test)

    % predict
    Y_pred = zeros(size(X_test,1),1);
    for i=1:size(X_test,1);
        Y_pred(i,1) = dt_value_bns(model, X_test(i,:));
    end
    
end

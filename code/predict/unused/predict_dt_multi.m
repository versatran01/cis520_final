function [Y_pred] = predict_dt_multi(model, X)

Y_pred = zeros(size(X,1),1);

for i=1:size(X,1)
    
    val = dt_value(model, X(i,:));
    val = sum(val .* [1 2 3 4 5]);
    
    Y_pred(i) = val;
end
end

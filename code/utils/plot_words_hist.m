
% get observations
X = train.counts;

% sum columns of X
totals = sum(X);

% upper limit on count
N = 250;

% percentages
perc = zeros(N, 1);

for n=1:N
    
    perc(n,1) = mean(totals >= n);
    
end

plot(1:n,perc);


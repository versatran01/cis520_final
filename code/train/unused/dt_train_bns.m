function root = dt_train_bns(X, Y, depth_limit)
% DT_TRAIN_BNS - Trains a multi-class decision tree classifier using BNS.
%
% Usage:
%
%    tree = dt_train_bns(X, Y, DEPTH_LIMIT)
%
% Returns a tree of maximum depth DEPTH_LIMIT learned using modified ID3
% algorithm. Assumes X is a N x D matrix of N examples with D features. Y
% must be a N x 1 {1, ..., K} vector of labels. 
%
% Returns a linked hierarchy of structs with the following fields:
%
%   node.terminal - whether or not this node is a terminal (leaf) node
%   node.fidx - feature index to split on
%   node.value - mean Y value
%   node.left - child node struct on left branch (f <= val)
%   node.right - child node struct on right branch (f > val)
%

% recursively split data to build tree.
root = split_node(X, Y, mean(Y), 1:size(X, 2), 0, depth_limit);

end

function [node] = split_node(X, Y, default_value, colidx, depth, depth_limit)
% Utility function called recursively; returns a node structure.
%    
%  [node] = split_node(X, Y, default_value, colidx, depth, depth_limit)
%  
%  inputs: 
%    default_value - the default value of the node if Z is empty
%    colidx - the indices of features (columns) under consideration
%    depth - current depth of the tree
%    depth_limit - maximum depth of the tree

%   Calculate the number of labels left to sort
num_labels = numel(unique(Y));

% The various cases at which we will return a terminal (leaf) node:
%    - we are at the maximum depth
%    - we have only one label left
%    - we have only a single (or no) examples left
%    - we have no features left to split on

if depth == depth_limit || num_labels == 1 || size(Y,1) <= 1 || numel(colidx) == 0
    node.terminal = true;
    node.fidx = [];
    if size(Y,1) == 0
        node.value = default_value;
    else
        node.value = mean(Y);
    end
    node.left = []; node.right = [];
    
    return;
end

node.terminal = false;

% Choose a feature to split on using BNS
bns = calc_bns(X,Y);
[~, node.fidx] = max(bns);

% Remove this feature from future consideration.
colidx(colidx==node.fidx) = [];

% Split the data based on this feature.
leftidx = find(X(:,node.fidx) > 0);
rightidx = find(X(:,node.fidx) == 0);

% Store the value of this node in case we later wish to use this node as a
% terminal node (i.e. pruning.)
node.value = mean(Y);

%fprintf('depth %d\n', depth);

% Recursively generate left and right branches.
node.left = split_node(X(leftidx, :), Y(leftidx, 1), node.value, colidx, depth+1, depth_limit);
node.right = split_node(X(rightidx, :), Y(rightidx, 1), node.value, colidx, depth+1, depth_limit);
end

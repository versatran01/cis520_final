function [fidx val max_ig] = dt_choose_feature_multi(X, Z, Xrange, colidx)
% DT_CHOOSE_FEATURE_MULTI - Selects feature with maximum multi-class IG.
%
% Usage:
% 
%   [FIDX FVAL MAX_IG] = dt_choose_feature(X, Z, XRANGE, COLIDX)
%
% Given N x D data X and N x K indicator labels Z, where X(:,j) can take on values in XRANGE{j}, chooses
% the split X(:,FIDX) <= VAL to maximize information gain MAX_IG. I.e., FIDX is
% the index (chosen from COLIDX) of the feature to split on with value
% FVAL. MAX_IG is the corresponding information gain of the feature split.
%
% Note: The relationship between Y and Z is that Y(i) = find(Z(i,:)).
% Z is the categorical representation of Y: Z(i,:) is a vector of all zeros
% except for a one in the Y(i)'th column.
% 
% Hint: It is easier to compute entropy, etc. when using Z instead of Y.
%
% SEE ALSO
%    DT_TRAIN_MULTI

% Must first calculate the entropy of all possible Y values
% Figure out the probability of any given Y ocurring
p_y = sum(Z) ./ size(Z, 1); % divided by N
H = multi_entropy(p_y');

% This will store the information gain for each possible x
% Note that numel(Xrange) should equal size(X,2) (# of features)
ig = zeros(numel(Xrange), 1);

% This will store the value of the feature used for splitting
split_vals = zeros(numel(Xrange), 1);

% Iterate over all features permitted
for i=colidx
    
    %   This is the range of possible values for X
    x_range = Xrange{i};
    
    %   Case: There is no range, so no information can be gained
    if (numel(Xrange{i})) == 1
        
        ig(i) = 0;
        split_vals(i) = 0;
        
        %   skip to next feature
        continue;
    end
    
    %   We take the range of possible values for feature i, and divide it
    %   into 30, at most
    r = linspace(double(Xrange{i}(1)), double(Xrange{i}(end)), min(30, numel(Xrange{i})));
    
    %   This call will create a 2-dimensional array. There will be a row
    %   for every value in X(:,i). Each column space will hold a 1 or 0, 
    %   reflecting if that row's x-value is less than or equal to the column's 
    %   r-value
    split_f = bsxfun(@le, X(:,i), r(1:end-1));
    
    %   Now we take the mean value of each column, thereby determining the
    %   fraction of Xs that fall beneath each r-value
    px = mean(split_f);
    
    %   We need to pick the best r-value for this feature
    %   To do this, we will calculate the conditional entropy of y on x
    %   We iterate over all possible values of y
    
    %   These are arrays of probabilities.
    %   There is a row for every possible Y-value.
    %   There is a column for every possible r-value.
    p_y_given_x = zeros(size(Z,2), size(px, 2));
    p_y_given_notx = zeros(size(Z,2), size(px, 2));
    
    for j = 1:size(Z,2) %   second dimension of Z is different Ys
        
       %    Column vector indicating if this is the selected Y value (1/0)
       y = Z(:,j);
       
       %    Now we can calculate conditional entropies.
       
       %    y_and_x is a 2D array. There is a row for every X-value in the
       %    training set (but only feature i). There is a column for every 
       %    r-value. Each space contains a 1 if that row's x-value is less 
       %    than or equal to the column's r-value, AND only if that x-value 
       %    produced the selected y value/label.
       y_and_x = bsxfun(@and, y, split_f);
       y_and_notx = bsxfun(@and, y, ~split_f);
       
       %    now we figure out P(y = 1 | X <= r)
       %
       %        p(y = 1 | X <= r) = p(y = 1 AND x <= r) / p(x <= r)
       %
       %    this works by summing columns into a row vector
       p_y_given_x(j,:) = sum(y_and_x) ./ sum(split_f);
       p_y_given_notx(j,:) = sum(y_and_notx) ./ sum(~split_f);
       
    end
    
    %   Now we may calculate the entropy on each column of probabilities
    %   in the p_y_given_x array.
    cond_H = px .* multi_entropy(p_y_given_x) + (1-px) .* multi_entropy(p_y_given_notx);
    
    %   Now choose the best r-value based on information gain
    [ig(i) best_split] = max(H - cond_H);
    split_vals(i) = r(best_split);
end

%   Finally choose the best feature
[max_ig fidx] = max(ig);
val = split_vals(fidx);

%   Done
end

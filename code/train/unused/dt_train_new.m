function root = dt_train_new(X, Y, vocab, depth_limit)
% DT_TRAIN_NEW - Trains a multi-class decision tree classifier.
%
% Usage:
%
%    tree = dt_train(X, Y, DEPTH_LIMIT)
%
% Returns a tree of maximum depth DEPTH_LIMIT learned using the ID3
% algorithm. Assumes X is a N x D matrix of N examples with D features. Y
% must be a N x 1 {1, ..., K} vector of labels. 
%
% Returns a linked hierarchy of structs with the following fields:
%
%   node.terminal - whether or not this node is a terminal (leaf) node
%   node.fidx, node.fval - the feature based test (is X(fidx) <= fval?)
%                          associated with this node
%   node.value - 1 x K vector of P(Y==K) as predicted by this node
%   node.left - child node struct on left branch (f <= val)
%   node.right - child node struct on right branch (f > val)
%

D = size(X,2);

% recursively split data to build tree
root = split_node(X, Y, mean(Y), 1:D, 0, vocab, depth_limit);

end

function [node] = split_node(X, Y, default_value, colidx, depth, vocab, depth_limit)
% Utility function called recursively; returns a node structure.
%    
%  [node] = split_node(X, Y, Xrange, default_value, colidx, depth, depth_limit)
%  
%  inputs: 
%    Xrange - cell array containing the range of values for each feature
%    default_value - the default value of the node if Z is empty
%    colidx - the indices of features (columns) under consideration
%    depth - current depth of the tree
%    depth_limit - maximum depth of the tree

num_labels = numel(unique(Y));

if depth == depth_limit || num_labels == 1 || numel(Y) <= 1 || numel(colidx) == 0
    %    - we are at the maximum depth
    %    - we have only one label left
    %    - we have only a single (or no) examples left
    %    - we have no features left to split on
    node.terminal = true;
    node.fidx = [];
    node.word = '';
    if numel(Y) == 0
        node.value = default_value;
    else
        node.value = mean(Y);
    end
    node.left = []; node.right = [];
    
    return;
else
    node.terminal = false;
end

% select feature
ig = fastig(X, Y, colidx, [1 2 3 4 5]);
[~, idx] = max(ig);
node.fidx = colidx(idx);
node.word = vocab{node.fidx};

% Remove this feature from future consideration.
colidx(idx) = [];

% Split the data based on this feature.
leftidx = find(X(:,node.fidx) == 0);  % <= node.fval);
rightidx = find(X(:,node.fidx) > 0); %  > node.fval);

% value of the node...
node.value = mean(Y);

% Recursively generate left and right branches.
node.left = split_node(X(leftidx, :), Y(leftidx, :), node.value, colidx, depth+1, vocab, depth_limit);
node.right = split_node(X(rightidx, :), Y(rightidx, :), node.value, colidx, depth+1, vocab, depth_limit);

end

%% BENCH_SUBMISSION.m Speed test the final submission code
startup;

model = init_model();

tic;
Ypred = zeros(size(Yvalid));
T = CTimeleft(numel(Yvalid));

% TEST: zero some rows of metadata
%Mvalid(10).text = {};
%Mvalid(11).text = {'test'};
%Mvalid(12).text = {'yo', 'mamma'};

for i=1:numel(Yvalid)
    T.timeleft();
    Ypred(i) = make_final_prediction(model,Xvalid(i,:),Mvalid(i));
end

fprintf('Ran %i samples in %.3f seconds\n', numel(Mvalid), toc);
fprintf('RMSE on validation set: %f\n', rms(Ypred - Yvalid));
